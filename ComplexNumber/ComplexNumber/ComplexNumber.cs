﻿using System;

namespace ComplexNumber
{
    class ComplexNumber
    {
        // fields to store real and imaginary parts
        private int real, img;
        public ComplexNumber() { }
        /// <summary>
        /// Constructor to set values
        /// </summary>
        /// <param name="realNum">Real part of complex number</param>
        /// <param name="imgNum">Imaginary part of complex number</param>
        public ComplexNumber(int realNum, int imgNum)
        {
            Real = realNum;
            Imaginary = imgNum;
        }
        /// <summary>
        /// Accessors for real part
        /// </summary>
        public int Real
        {
            get => real;
            set => real = value;
        }
        /// <summary>
        /// Accessors for Imaginary part
        /// </summary>
        public int Imaginary
        {
            get => img;
            set => img = value;
        }
        /// <summary>
        /// To find magnitude of a real number
        /// </summary>
        /// <param name="real">Real number</param>
        /// <param name="img">Imaginary number</param>
        /// <returns>double value represents Magnitude</returns>
        public double Magnitude()
        {
            return Math.Sqrt(Math.Pow(Real, 2) + Math.Pow(Imaginary, 2));
        }
        /// <summary>
        /// Addition of two complex numbers
        /// </summary>
        /// <param name="obj1">Object of first number</param>
        /// <param name="obj2">Object of second number</param>
        /// <returns></returns>
        public void Add(ComplexNumber obj1, ComplexNumber obj2)
        {
            Real = obj1.Real + obj2.Real;
            Imaginary = obj1.Imaginary + obj2.Imaginary;
        }
        /// <summary>
        /// Display method to print the complex number
        /// </summary>
        public void display()
        {
            Console.Write(Real + " + " + Imaginary+"i");
        }
        static void Main(string[] args)
        {
            int real, img;
            Console.WriteLine("Press 1 to find Magnitude of a Complex number\nPress 2 for Addition of 2 complex numbers");
            int opt = Convert.ToInt32(Console.ReadLine());
            if (opt == 1)
            {
                Console.WriteLine("Enter Real Number of a Complex");
                real = Convert.ToInt32(Console.ReadLine());
                Console.WriteLine("Enter Imaginary Number of a Complex");
                img = Convert.ToInt32(Console.ReadLine());
                ComplexNumber complex = new ComplexNumber(real, img);
                Console.Write("Magnitude of ");
                complex.display();
                Console.Write(" is " + String.Format("{0:0.##}", complex.Magnitude()));
            }
            else if (opt == 2)
            {
                Console.WriteLine("Enter Real Number of first Complex");
                real = Convert.ToInt32(Console.ReadLine());
                Console.WriteLine("Enter Imaginary Number of first Complex");
                img = Convert.ToInt32(Console.ReadLine());
                ComplexNumber firstComplex = new ComplexNumber(real, img);
                Console.WriteLine("Enter Real Number of second Complex");
                real = Convert.ToInt32(Console.ReadLine());
                Console.WriteLine("Enter Imaginary Number of second Complex");
                img = Convert.ToInt32(Console.ReadLine());
                ComplexNumber secondComplex = new ComplexNumber(real, img);
                ComplexNumber thirdComplex = new ComplexNumber();
                thirdComplex.Add(firstComplex, secondComplex);
                Console.Write("Sum of ");
                firstComplex.display();
                Console.Write(" and ");
                secondComplex.display();
                Console.Write(" is ");
                thirdComplex.display();
            }
            else
                Console.WriteLine("Not a valid selection !");
        }
    }
}
